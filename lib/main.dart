import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/services/base.dart';
import 'widgets.dart';
import 'dart:async';
import 'dart:isolate';
import 'dart:ui';
import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_geofence/geofence.dart';
import 'package:flutter/material.dart';

import 'package:geofencing/geofencing.dart';
void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class AppState extends InheritedWidget {
  const AppState({
    Key key,
    this.mode,
    Widget child,
  }) : assert(mode != null),
        assert(child != null),
        super(key: key, child: child);

  final Geocoding mode;

  static AppState of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppState);
  }

  @override
  bool updateShouldNotify(AppState old) => mode != old.mode;
}

//Tracking view
class TrackingView extends StatefulWidget {

  TrackingView();

  @override
  _TrackingViewState createState() => new _TrackingViewState();
}

class _TrackingViewState extends State<TrackingView> {

  _TrackingViewState();

  TextEditingController _c ;
  String _platformVersion = 'Unknown';
  String _text = "initial";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    _c = new TextEditingController();
    super.initState();
    initPlatformState();

// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  //  var initializationSettingsAndroid =
   // new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS =
    IOSInitializationSettings(onDidReceiveLocalNotification: null);
    // var initializationSettings = InitializationSettings(
    //     initializationSettingsAndroid, initializationSettingsIOS);
    // flutterLocalNotificationsPlugin.initialize(initializationSettings,
    //     onSelectNotification: null);
  }
  @override
  void dispose(){
    _c?.dispose();
    super.dispose();
  }
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    Geofence.initialize();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(

        body: ListView(
          children: <Widget>[
            RaisedButton(
              child: Text("Request Permissions"),
              onPressed: () {
                Geofence.requestPermissions();
              },
            ),
            RaisedButton(
                child: Text("Clock in to get user location"),
                onPressed: () {
                  Geofence.getCurrentLocation().then((coordinate) {
                    print(
                        "great got latitude: ${coordinate.latitude} and longitude: ${coordinate.longitude}");
                    setState((){
                      _text = "great got latitude: ${coordinate.latitude} and longitude: ${coordinate.longitude}";
                    });
                  });
                }),
            RaisedButton(
                child: Text("Listen to background updates"),
                onPressed: () {
                  Geofence.startListeningForLocationChanges();
                  Geofence.backgroundLocationUpdated.stream.listen((event) {
                    print("You moved significantly,"+"a significant location change just happened.");
                  });
                }),
            RaisedButton(
                child: Text("Stop listening to background updates"),
                onPressed: () {
                  Geofence.stopListeningForLocationChanges();
                }),
           Text(_text.toString()),
          ],
        ),

      ),
    );
  }}

class GeocodeView extends StatefulWidget {

  GeocodeView();

  @override
  _GeocodeViewState createState() => new _GeocodeViewState();
}

class _GeocodeViewState extends State<GeocodeView> {

  _GeocodeViewState();

  final TextEditingController _controller = new TextEditingController();

  List<Address> results = [];

  bool isLoading = false;

  Future search() async {

    this.setState(() {
      this.isLoading = true;
    });

    try{
      var geocoding = AppState.of(context).mode;
      var results = await geocoding.findAddressesFromQuery(_controller.text);
      this.setState(() {
        this.results = results;
      });
    }
    catch(e) {
      print("Error occured: $e");
    }
    finally {
      this.setState(() {
        this.isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
        children: <Widget>[
          new Card(
            child: new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextField(
                      controller: _controller,
                      decoration: new InputDecoration(hintText: "Enter an address"),
                    ),
                  ),
                  new IconButton(icon: new Icon(Icons.search), onPressed: () => search())
                ],
              ),
            ),
          ),
          new Expanded(child: new AddressListView(this.isLoading, this.results)),
        ]);
  }
}

class ReverseGeocodeView extends StatefulWidget {

  ReverseGeocodeView();

  @override
  _ReverseGeocodeViewState createState() => new _ReverseGeocodeViewState();
}

class _ReverseGeocodeViewState extends State<ReverseGeocodeView> {

  final TextEditingController _controllerLongitude = new TextEditingController();
  final TextEditingController _controllerLatitude = new TextEditingController();

  _ReverseGeocodeViewState();

  List<Address> results = [];

  bool isLoading = false;

  Future search() async {


    this.setState(() {
      this.isLoading = true;
    });

    try{
      var geocoding = AppState.of(context).mode;
      var longitude = double.parse(_controllerLongitude.text);
      var latitude = double.parse(_controllerLatitude.text);
      var results = await geocoding.findAddressesFromCoordinates(new Coordinates(latitude, longitude));
      this.setState(() {
        this.results = results;
      });
    }
    catch(e) {
      print("Error occured: $e");
    }
    finally {
      this.setState(() {
        this.isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
        children: <Widget>[
          new Card(
            child: new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Column(
                      children: <Widget>[
                        new TextField(
                          controller: _controllerLatitude,
                          decoration: new InputDecoration(hintText: "Latitude"),
                        ),
                        new TextField(
                          controller: _controllerLongitude,
                          decoration: new InputDecoration(hintText: "Longitude"),
                        ),
                      ],
                    ),
                  ),
                  new IconButton(icon: new Icon(Icons.search), onPressed: () => search())
                ],
              ),
            ),
          ),
          new Expanded(child: new AddressListView(this.isLoading, this.results)),
        ]);
  }
}

class _MyAppState extends State<MyApp> {

  Geocoding geocoding = Geocoder.local;

  final Map<String, Geocoding> modes = {
    "Local" : Geocoder.local,
    "Google (distant)" : Geocoder.google("AIzaSyCPaTH9J_i6km1hHwVAR5t9Yv8mEkYEGzI"),
  };

  void _changeMode(Geocoding mode) {
    this.setState(() {
      geocoding = mode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new AppState(
      mode: this.geocoding,
      child: new MaterialApp(
        home: new DefaultTabController(
          length: 3,
          child: new Scaffold(
            appBar: new AppBar(
              title: new Text('Location Tracking'),
              actions: <Widget>[
                new PopupMenuButton<Geocoding>( // overflow menu
                  onSelected: _changeMode,
                  itemBuilder: (BuildContext context) {
                    return modes.keys.map((String mode) {
                      return new CheckedPopupMenuItem<Geocoding>(
                        checked: modes[mode] == this.geocoding,
                        value: modes[mode],
                        child: new Text(mode),
                      );
                    }).toList();
                  },
                ),
              ],
              bottom: new TabBar(
                tabs:  [
                  new Tab (
                    text: "Tracking",
                    icon: new Icon(Icons.location_history),
                  ),
                  new Tab(
                    text: "Query",
                    icon: new Icon(Icons.search),
                  ),
                  new Tab(
                    text: "Coordinates",
                    icon: new Icon(Icons.pin_drop),
                  ),
                ],
              ),
            ),
            body: new TabBarView(
                children: <Widget>[
                new TrackingView(),
                  new GeocodeView(),
                  new ReverseGeocodeView(),
                ]),
          ),
        ),
      ),
    );
  }
}